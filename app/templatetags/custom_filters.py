from django.template import Library

register = Library()

@register.filter(name='range')
def range_filter(val, arg=False):
    if isinstance(arg, bool):
        return range(val)
    if isinstance(arg, int):
        return range(val, arg)

@register.filter(name='div')
def div(val, arg):
    return val//arg

