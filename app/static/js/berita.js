
function getUrlRequest(){
  var result = "/berita/?ordering=" 
  result = result + (descending?"-":"");
  result = result + ordering;
  result = result + "&showing="+showing*10;
  result = result +"&page_num="+page_num;
  return result;
}
function request(){
  //fungsi buat minta request
  $.ajax({
    url: getUrlRequest(),
    success: function(xhr){
      $('.card-deck a').each(function (index)
      {
        if (index < xhr.data.length){
        $(this).attr('href', '/berita/view?id='+xhr.data[index].id);
        $(this).find('.author').html(xhr.data[index].author);
        $(this).find('.date').html(xhr.data[index].date);
        $(this).find('.text-primary').html(xhr.data[index].title);
        $(this).find('.text-dark span').html(xhr.data[index].viewed);
        $(this).find('.text-danger span').html(xhr.data[index].liked);
        $(this).find('.text-success span').html(xhr.data[index].comments);
        $(this).css({'opacity':0, 'visibility': 'visible'}).animate({opacity: 1}, 'slow');
        };
      })
    },
    complete: enableButton,
})
}
function hideCard(){
  $('.card-deck a').css({'visibility': 'hidden', 'opacity':1})
}
function disableButton(){
  //showing button
  $('#showing button').addClass('disabled');

  //ordering button
  $('#ordering button').addClass('disabled');

  //pagination
  $('.pagination li:not(.active, .disabled)').find('a').off();

  //button up-down
  $('#sort-by').off();
}
function reformat(num)
{
  return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
function paginationFunc(){
  disableButton();
  hideCard();
  switch($(this).attr('data-val'))
  {
    case "first":
      if (page_num==Math.ceil(banyakElemen/(showing*10)))
      {
      $('.page-item a[data-val=\"next\"], .page-item a[data-val=\"last\"]').parent().removeClass('disabled');
      }
      toFirstPage();
      break;
    case "prev":
      if (page_num==Math.ceil(banyakElemen/(showing*10)))
      {//from last page
      $('.page-item a[data-val=\"next\"], .page-item a[data-val=\"last\"]').parent().removeClass('disabled');
      }
      page_num--;
      $('.page-item a[data-val=\"current\"]').html(reformat(page_num));
      if (page_num==1)
      {
        $('.page-item a[data-val=\"first\"], .page-item a[data-val=\"prev\"]').parent().addClass('disabled');
      }
    break;
    case "next":
      if (page_num==1)
      {
        $('.page-item a[data-val=\"prev\"], .page-item a[data-val=\"first\"]').parent().removeClass('disabled');
      }
      page_num++;
      $('.page-item a[data-val=\"current\"]').html(reformat(page_num));
      if (page_num==Math.ceil(banyakElemen/(showing*10)))
      {
        $('.page-item a[data-val=\"next\"], .page-item a[data-val=\"last\"]').parent().addClass('disabled');
      }
      break;
    case "last":
      if (page_num==1)
      {
        $('.page-item a[data-val=\"first\"], .page-item a[data-val=\"prev\"]').parent().removeClass('disabled');
      }
      page_num = Math.ceil(banyakElemen/(showing*10));
      $('.page-item a[data-val=\"current\"]').html(reformat(page_num));
      $('.page-item a[data-val=\"next\"], .page-item a[data-val=\"last\"]').parent().addClass('disabled');
      break;
  }
  request();
}

function showingNumFunc(){
  if(!$(this).hasClass('active')){
    disableButton();
    hideCard();
  $('#showing .dropdown-item.active').removeClass('active');
  $(this).addClass('active');
  var newVal = parseInt($(this).attr('data-val'));
  if (newVal > showing)
  {//kasus ada elemen baru
    for (let i=showing+1; i<=newVal;++i)
    {
      $('.card-deck a[data-kelompok=\"'+(i-1)+'\"').css({'display': 'inline-block', 'visiblity': 'hidden'})
    }
  }
  else{//kasus ga ada elemen baru
    for (let i=newVal; i<showing;++i)
    {
      $('.card-deck a[data-kelompok=\"'+i+'\"').css({'display': 'none', 'visiblity': 'visible'})
    }
  }
  showing = newVal;
  for (let i=1; i<=showing;++i)
  {
    $('.card-deck a').css({'visibility': 'hidden'})
  }
  toFirstPage();
  request();
}}
function orderingDropdownFunc(){
  if (!$(this).hasClass('active')){
    disableButton();
    hideCard();
  $('#ordering .dropdown-item.active').removeClass('active');
  $(this).addClass('active');
  ordering = $(this).attr('data-val');
  toFirstPage();
  request();
}}

function ascendingOrDescending(){
  disableButton();
  hideCard();
  if (descending)
  {
    $(this).find('.fas').removeClass('fa-caret-up').addClass('fa-caret-down');
  }
  else{
    $(this).find('.fas').removeClass('fa-caret-down').addClass('fa-caret-up');
  }
  descending = !descending;
  toFirstPage();
  request();
}


function toFirstPage(){
  page_num=1;
  if (showing*10 >= banyakElemen)
  {
    $('.page-item a[data-val=\"next\"], .page-item a[data-val=\"last\"]').parent().addClass('disabled');
  }
  else{
    $('.page-item a[data-val=\"next\"], .page-item a[data-val=\"last\"]').parent().removeClass('disabled');
  }
  $('.page-item a[data-val=\"current\"]').html(reformat(page_num));
  $('.page-item a[data-val=\"first\"]').parent().addClass('disabled');
  $('.page-item a[data-val=\"prev\"]').parent().addClass('disabled');
}
function enableButton()
{
  //enable showing button
  $('#showing button').removeClass('disabled');

  //ordering button
  $('#ordering button').removeClass('disabled');

  //pagination
  $('.pagination li:not(.active, .disabled)').find('a').on('click',paginationFunc);

  //button up-down
  $('#sort-by').on('click', ascendingOrDescending);

}
function main(){

  //disable button & recalculate page
  disableButton();
  toFirstPage();
  //Assign event handler
  $('#showing .dropdown-item').on('click',showingNumFunc);
  $('#ordering .dropdown-item').on('click', orderingDropdownFunc);


  //minta request
  request();

  //enable button
  enableButton();
}

$(document).ready(main);