let questions = [
    {
    numb: 1,
    question: "Apa nama ilmiah dari virus corona?",
    answer: "B. SARS-CoV-2",
    options: [
      "A. COVID-19",
      "B. SARS-CoV-2",
      "C. Novel Coronavirus",
      "D. Coronavirus"
    ]
  },
    {
    numb: 2,
    question: "Apa kepanjangan dari ODP?",
    answer: "A. Orang Dalam Pemantauan",
    options: [
      "A. Orang Dalam Pemantauan",
      "B. Orang Dengan Penyakit",
      "C. Orang Dalam Pengawasan",
      "D. Orang Dalam Perlindungan"
    ]
  },
    {
    numb: 3,
    question: "Menjaga jarak aman secara fisik disebut juga?",
    answer: "D. Physical distancing",
    options: [
      "A. Social distancing",
      "B. Karantina",
      "C. Long distance",
      "D. Physical distancing"
    ]
  },
    {
    numb: 4,
    question: "Apa istilah mewabahnya suatu virus?",
    answer: "C. Pandemik",
    options: [
      "A. Epidemik",
      "B. Endemik",
      "C. Pandemik",
      "D. Epidermik"
    ]
  },
    {
    numb: 5,
    question: "Lockdown dalam bahasa Indonesia disebut dengan?",
    answer: "A. Karantina wilayah",
    options: [
      "A. Karantina wilayah",
      "B. Penutupan",
      "C. Penguncian",
      "D. Isolasi"
    ]
  }
];