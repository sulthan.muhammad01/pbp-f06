var el = document.getElementById("un");
    el.addEventListener("keypress", function(event) {
      if (event.key === "Enter") {
        alert("Click button");
        event.preventDefault();
      }
    });

var el = document.getElementById("uotp");
    el.addEventListener("keypress", function(event) {
      if (event.key === "Enter") {
        alert("Click button");
        event.preventDefault();
      }
    });

function getpass(){
    let username=$("#un").val();
    $.ajax({
        url:"../reset_password",
        type:"GET",
        data:{username:username},
        success:function(data){
            if(data.status=="failed"){
                $("#result").html("<p class='alert alert-danger'>No user registred with this username</p>");
            }
            else if(data.status=="error"){
                $("#result").html("<p class='alert alert-danger'>Could not send email to "+data.email+" Something went wrong!!!</p>");
            }
            else if(data.status=="sent"){
                $("#result").html("<p class='alert alert-success'>An OTP sent to your registred Email ID: "+data.email+"</p>");
                $("#continue").hide();
                $("#afterotp").slideDown(1000);
                $("#realotp").val(data.rotp)
            }
        }
    })
}

function matchotp(){
    uotp = $("#uotp").val();
    rotp = $("#realotp").val();

    if(uotp==rotp){
        $("#afterotp").hide()
        $("#changepass").fadeIn(1000);
        $("#result").html("<p class='alert alert-success'>Validation Success!!!</p>");
    }else{
        $("#result").html("<p class='alert alert-danger'>Incorrect OTP</p>");
    }
}

function refreshPage(){
    window.location.reload();
} 