from django.urls import path
from .views import addQuestion, delete_user, index, login_request, logout_request, register_request, provinsi, berita, profile, setting, change_password, delete_user, rumahsakit, view_berita, forgotpassword, reset_password, kuis

urlpatterns = [
    path('', index, name='index'),
    path('login/', login_request, name='login'),
    path('register/', register_request, name='register'),
    path('logout/', logout_request, name='logout'),
    path('provinsi/', provinsi, name='provinsi'),
    path('berita/', berita, name='berita'),
    path('profil/', profile, name='profil'),
    path('setting/', setting, name='setting'),
    path('password/', change_password, name='password'),
    path('add-question/', addQuestion, name='add-question'),
    path('delete-user/', delete_user, name='delete-user'),
    path('rumahsakit/', rumahsakit, name='rumahsakit'),
    path('berita/view', view_berita, name="view-berita"),
    path('forgot-password/', forgotpassword, name='forgot-password'),
    path('reset_password/', reset_password, name='reset_password'),
    path('kuis/', kuis, name='kuis'),
]
