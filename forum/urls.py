from django.urls import path
from .views import forum, delete_post, reply_post, replies

urlpatterns = [
    path('forum/', forum, name='forum'),
    path('delete_post/', delete_post, name='delete_post'),
    path('reply_post/', reply_post, name='reply_post'),
    path('replies/<id>', replies, name='replies/<id>'),
]
