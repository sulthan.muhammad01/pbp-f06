from django.db import models

# Create your models here.


class Post(models.Model):
    username = models.CharField(max_length=100)
    title = models.CharField(max_length=100)
    text = models.CharField(max_length=10000)


class Reply(models.Model):
    reply = models.CharField(max_length=10000)
    to = models.CharField(max_length=10000)
    From = models.CharField(max_length=100)
