from django.shortcuts import render, redirect
from .forms import PostForm, ReplyForm
from django.contrib import messages
from django.http import HttpResponse
from .models import Post, Reply
from django.core import serializers
# Create your views here.


def forum(request):
    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            messages.success(request, "Postingan berhasil ditambahkan.")
            form.save()
    context = {}
    context['posts'] = Post.objects.all()
    if request.user.is_authenticated:
        context['username'] = request.user.username
    return render(request, 'forum.html', context)


def delete_post(request):
    if request.method == "POST":
        id = request.POST['pk']
        obj = Post.objects.get(pk=id)
        obj.delete()
        messages.success(request, "Postingan berhasil dihapus.")
        return redirect('/forum')
    return redirect("/")


def reply_post(request):
    if request.method == "POST":
        form = ReplyForm(request.POST)
        if form.is_valid():
            form.save()
        messages.success(request, "Balasan anda berhasil ditambahkan.")
        return redirect('/forum')
    return redirect("/")


def replies(request, id):
    replies = serializers.serialize("json", Reply.objects.filter(to=id))
    return HttpResponse(replies)
