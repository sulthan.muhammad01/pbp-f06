from django import forms
from .models import Post, Reply

# Create your forms here.


class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ("username", "title", "text")


class ReplyForm(forms.ModelForm):
    class Meta:
        model = Reply
        fields = ("reply", "to", "From")
